package com.example.tareaconstraintlayout;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button botonAceptar;
    CheckBox checkBox;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        botonAceptar = findViewById(R.id.buttonAceptar);
        checkBox = findViewById(R.id.checkBoxVista);

        buttonListeners();
    }

    private void buttonListeners() {
        botonAceptar.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.buttonAceptar:
                if (checkBox.isChecked()) {
                    Toast.makeText(getApplicationContext(),
                            "Excelente eres un auténtico seriófilo",
                            Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getApplicationContext(),
                            "Imperdonable, debes ver esta serie",
                            Toast.LENGTH_LONG).show();
                }
        }
    }
}